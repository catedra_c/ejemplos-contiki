Separar el programa en módulos
==============================

Es posible separar la implementación en varios archivos, para esto simplemente
hay que agregar en el Makefile la variable `PROJECT_SOURCEFILES` con los
archivos `.c` que se desee linkear con el programa principal.

Por ejemplo para compilar `hello-world.c` usando el módulo `saludar.c`:
```
CONTIKI_PROJECT = hello-world
PROJECT_SOURCEFILES = saludar.c
all: $(CONTIKI_PROJECT)

CONTIKI ?= /opt/contiki
include $(CONTIKI)/Makefile.include
```
