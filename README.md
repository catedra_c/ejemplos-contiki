Ejemplos
========

Los ejemplos se presentan en este README en el orden en el que se recomienda
probarlos.
A la derecha de cada ejemplo aparecen las páginas del libro
(http://wireless.ictp.it/Papers/InternetdelasCosas.pdf) que explican
los conceptos volcados en esos ejemplos.

La carpeta que contiene cada ejemplo además tiene un README.md con
más explicaciones sobre como probarlos.

Programa básico:
- hello-world/ páginas 56 a 59

Lectura de sensores digitales:
- leer-sensores/ páginas 61 y 62, también páginas 67 a 69

Envío/recepción de datos con UDP y uso de timers con `etimer`:
- envio-recepcion-con-udp páginas 99 a 101, 60 y 61

Cómo separar el programa en varios archivos:
- modularizacion/

Ejemplo (compilado) de envío y recepción de datos en el formato del trabajo:
- recibe-formato-tpi/
