Introducción
------------

Los sensores que usaremos serán:

- SHT25: Sensor de temperatura y humedad
- BMP085: Sensor de temperatura y presión

El primer paso será agregar soporte para el BMP085 a Contiki ya
que actualmente no cuenta con un "driver" para este dispositivo.
Para agregar el driver a Contiki es necesario copiar los archivos
del directorio `driver` a un directorio específico de Contiki:

```
sudo cp driver/bmp* /opt/contiki/platform/z1/dev/
```
Una vez hecho esto podremos usar el sensor de presión BMP085.

Uso
---

Para estas pruebas es necesario contar con las motas Z1 o bien utilizar
el simulador Cooja, eligiendo la plataforma z1 como `TARGET` al compilar.
Estos ejemplos no funcionan en modo "native".

Para usar los sensores es necesario incluir en el programa:
```
#include "dev/sht25.h"
#include "dev/bmpx8x.h"
```

Mientras que en el `Makefile` será necesario agregar los archivos `.c` que
implementan los "drivers" de estos dispositivos en la variable
`CONTIKI_SOURCE_FILES`:

```
CONTIKI_SOURCEFILES += sht25.c bmpx8x.c
```
